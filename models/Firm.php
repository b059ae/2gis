<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "firms".
 *
 * @property integer $id
 * @property string $gis2_id
 * @property string $region_id
 * @property string $name
 * @property string $address
 * @property string $json
 * @property integer $created_at
 * @property integer $updated_at
 */
class Firm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'firm';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gis2_id', 'region_id', 'json'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'gis2_id' => '2gis ID',
            'region_id' => 'Region ID',
            'name' => 'Name',
            'address' => 'Address',
            'json' => 'Json',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
