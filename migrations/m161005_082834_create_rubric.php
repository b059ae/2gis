<?php

use yii\db\Migration;

class m161005_082834_create_rubric extends Migration
{
    public function up()
    {
        //рубрики фирм
        $this->createTable('rubric', [
            'id' => $this->bigPrimaryKey(),
            'parent_id' =>$this->bigInteger(),
            'name' => $this->string(),
            'region_id' => $this->integer(),
            'created_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

//        $this->createIndex('ix_rubric_parent_id', 'rubric', 'parent_id');
//        $this->addForeignKey('fk_rubric_to_rubric', 'rubric', 'parent_id', 'rubric', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('rubric');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
