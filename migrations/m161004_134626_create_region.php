<?php

use yii\db\Migration;

class m161004_134626_create_region extends Migration
{
    public function up()
    {
// Таблица персональной информации на Пользователя
        $this->createTable('region', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('region');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
