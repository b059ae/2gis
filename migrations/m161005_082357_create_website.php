<?php

use yii\db\Migration;

class m161005_082357_create_website extends Migration
{
    public function up()
    {
        //сайты фирм
        $this->createTable('website', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'firm_id' => $this->integer(),
            'created_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        
        $this->createIndex('ix_website_firm_id', 'website', 'firm_id');
        $this->addForeignKey('fk_website_to_firm', 'website', 'firm_id', 'firm', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('website');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
