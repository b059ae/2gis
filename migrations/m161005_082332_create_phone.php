<?php

use yii\db\Migration;

class m161005_082332_create_phone extends Migration
{
    public function up()
    {
        //телефоны фирм
        $this->createTable('phone', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'firm_id' => $this->integer(),
            'created_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex('ix_phone_firm_id', 'phone', 'firm_id');
        $this->addForeignKey('fk_phone_to_firm', 'phone', 'firm_id', 'firm', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('phone');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
