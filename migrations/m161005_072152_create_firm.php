<?php

use yii\db\Migration;

class m161005_072152_create_firm extends Migration
{
    public function up()
    {
        // Таблица информации о фирмах
        $this->createTable('firm', [
            'id' => $this->primaryKey(),
            'gis2_id' => $this->bigInteger(),
            'region_id' => $this->integer(),
            'name' => $this->string()->unique(),
            'address' => $this->string(),
            'json' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex('ix_region_id', 'firm', 'region_id');
        $this->addForeignKey('fk_firm_to_region', 'firm', 'region_id', 'region', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('firm');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
