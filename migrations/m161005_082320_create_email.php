<?php

use yii\db\Migration;

class m161005_082320_create_email extends Migration
{
    public function up()
    {
        //почта фирм
        $this->createTable('email', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'firm_id' => $this->integer(),
            'created_at' => $this->integer(),
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex('ix_email_firm_id', 'email', 'firm_id');
        $this->addForeignKey('fk_email_to_firm', 'email', 'firm_id', 'firm', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('email');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
