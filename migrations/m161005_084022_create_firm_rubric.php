<?php

use yii\db\Migration;

class m161005_084022_create_firm_rubric extends Migration
{
    public function up()
    {
        //результаты телефонов
        $this->createTable('firm_rubric', [
            'id' => $this->primaryKey(),
            'firm_id' => $this->integer(),
            'rubric_id' => $this->bigInteger()
        ],  'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->createIndex('ix_rubric_id', 'firm_rubric', 'rubric_id');
        $this->addForeignKey('fk_firm_rubric_to_rubric', 'firm_rubric', 'rubric_id', 'rubric', 'id');
        $this->createIndex('ix_firm_id', 'firm_rubric', 'firm_id');
        $this->addForeignKey('fk_firm_rubric_to_firm', 'firm_rubric', 'firm_id', 'firm', 'id');
    }

    public function down()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS=0');
        $this->dropTable('firm_rubric');
        $this->execute('SET FOREIGN_KEY_CHECKS=1');
    }
}
