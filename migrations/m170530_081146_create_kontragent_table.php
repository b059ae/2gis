<?php

use yii\db\Migration;

/**
 * Handles the creation of table `kontragent`.
 */
class m170530_081146_create_kontragent_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('kontragent', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(),
            'short_name' => $this->string(),
            'inn' => $this->string(),
            'kpp' => $this->string(),
            'ogrn' => $this->string(),
            'address' => $this->string(),
            'manager' => $this->string(),
            'phone' => $this->string(),
            'email' => $this->string(),
            'status' => $this->string(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('kontragent');
    }
}
