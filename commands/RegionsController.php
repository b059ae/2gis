<?php

namespace app\commands;

use yii\console\Controller;
use yii\helpers\VarDumper;

/**
 * Парсер регионов с сайта 2gis.ru
 *
 */
class RegionsController extends Controller
{
    public $tableName = 'region';
    /**
     * @var string Название компонента БД
     */
    public $db = 'db';
    /**
     * Получение списка регионов
     *
     * {
     * "name": "Орёл",
     * "id": "71",
     * "type": "region"
     * }
     */
    protected function get()
    {
        $url = "http://catalog.api.2gis.ru/2.0/region/list?key={apiKey}";

        return \Yii::$app->parser->exec($url);
    }

    public function actionPrint()
    {
        $regions = $this->get();
        echo VarDumper::dumpAsString($regions);
    }

    /**
     * Создание справочника регионов
     *
     */
    public function actionInsert()
    {
        $regions = $this->get();

        $connection = \Yii::$app->{$this->db};

        $time = time();
        $connection->createCommand()->batchInsert($this->tableName, ['id', 'name', 'created_at'], array_map(function ($item) use ($time) {
            return [
                $item->id,
                $item->name,
                $time
            ];
        }, $regions->result->items))->execute();
    }


}
