<?php

namespace app\commands;

use app\components\RubricHelper;
use yii\console\Controller;
use app\models\Region;


/**
 * Парсер рубрик с сайта 2gis.ru
 *
 */
class RubricsController extends Controller
{
    /**
     * @var integer ID региона
     */
    public $region_id;
    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return [
            'region_id',
        ];
    }
    /**
     * Создание справочника рубрик
     *
     */
    public function actionIndex()
    {
        if (empty($this->region_id)) {
            $regions = Region::find()->all();
            foreach ($regions as $region) {
                $region_id = $region->id;
                $rubric = new RubricHelper(['region_id' => $region_id]);
                $rubric->fillRubrics();
            }
        } else{
            $rubric = new RubricHelper(['region_id' => $this->region_id]);
            $rubric->fillRubrics();
        }
    }

}
