<?php

namespace app\commands;

use app\components\FirmsHelper;
use app\components\SaveToCSV;
use app\models\Firm;
use app\models\Region;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;


/**
 * Парсер фирм с сайта 2gis.ru
 *
 */
class FirmsController extends Controller
{
    /**
     * @var string ID рубрики
     */
    public $rubric_id;
    /**
     * @var integer ID региона
     */
    public $region_id;

    /**
     * Получение списка фирм
     *
     */

    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return [
            'rubric_id',
            'region_id',
        ];
    }

    public function beforeAction($action)
    {
        if (empty($this->rubric_id) || empty($this->region_id)){
            $this->stdout("Введите rubric_id и region_id\n", Console::FG_RED);
            exit();
        }
        return parent::beforeAction($action);
    }

    /**
     * Создание справочника фирм
     *
     */
    public function actionIndex()
    {
        $firm = new FirmsHelper(['region_id' => $this->region_id, 'rubric_id' => $this->rubric_id]);
        $firm->insertFirm();
    }
}

