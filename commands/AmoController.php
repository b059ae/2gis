<?php

namespace app\commands;

use app\helpers\AmoHelper;
use app\models\Email;
use app\models\Firm;
use app\models\FirmRubric;
use app\models\Phone;
use app\models\Region;
use app\models\Rubric;
use app\models\Website;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;


/**
 * Добавление лидов на битрикс
 *
 */
class AmoController extends Controller
{
    /**
     * @var string Слово или фраза для поиска рубрики
     */
    public $item;

    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return [
            'item',
        ];
    }

    /**
     * Проверка перед выполнением action на наличие значения для поиска
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (empty($this->item)){
            $this->stdout("Введите слово или фразу для поиска рубрики (--item=)\n", Console::FG_RED);
            exit();
        }
        return parent::beforeAction($action);
    }


    public function actionInsert()
    {
        // Выгрузить все компании из-амо
        \Yii::$app->amo->integrate();
        \Yii::$app->amo->getCompany();
        $regions = Region::find()->all();
        $time = time();
        foreach ($regions as $region) {
            $region_id = $region->id;
            $rubrics = Rubric::find()
                ->andWhere(['like', 'name', '%' . $this->item . '%', false])
                ->andWhere(['region_id' => $region_id])
                ->all();
            foreach ($rubrics as $rubric) {
                $rubric_id = $rubric->id;
                $amo = new AmoHelper(['region_id' => $region_id, 'rubric_id' => $rubric_id, 'time' => $time]);
                $amo->send();
            }
        }
    }
}

