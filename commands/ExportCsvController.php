<?php

namespace app\commands;

use app\components\ExportCsvHelper;
use app\models\Region;
use app\models\Rubric;
use yii\console\Controller;
use yii\helpers\Console;


/**
 *
 */
class ExportCsvController extends Controller
{
    /**
     * @var string Слово или фраза для поиска рубрики
     */
    public $item;

    /**
     * @param string $actionID
     * @return array
     */
    public function options($actionID)
    {
        return [
            'item',
        ];
    }

    /**
     * Проверка перед выполнением action на наличие значения для поиска
     * @param \yii\base\Action $action
     * @return bool
     */
    public function beforeAction($action)
    {
        if (empty($this->item)){
            $this->stdout("Введите слово или фразу для поиска рубрики (--item=)\n", Console::FG_RED);
            exit();
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $regions = Region::find()->all();
        $time = time();
        foreach ($regions as $region){
            $region_id = $region->id;
            $rubrics = Rubric::find()
                ->andWhere(['like', 'name', '%'.$this->item.'%', false])
                ->andWhere(['region_id' => $region_id])
                ->all();
            foreach ($rubrics as $rubric){
                $rubric_id = $rubric->id;
                $csv = new ExportCsvHelper(['region_id' => $region_id, 'rubric_id' => $rubric_id, 'time' => $time]);
                $csv->csv();
            }

        }
    }
}

