<?php

namespace app\commands;

use app\models\Email;
use app\models\Firm;
use app\models\FirmRubric;
use app\models\Phone;
use app\models\Rubric;
use app\models\Website;
use yii\base\Exception;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\VarDumper;


/**
 * Добавление лидов на битрикс
 *
 */
class LidsController extends Controller
{

    public function actionInsert()
    {
        $login = "dgolubenko@credit-history24.ru";
        $password = "331150";
        $firms = Firm::find()->all();
        foreach ($firms as $firm) {
        $phone = Phone::find()->where(['firm_id' => $firm->id])->all();
        $website = Website::find()->where(['firm_id' => $firm->id])->one();
        $email = Email::find()->where(['firm_id' => $firm->id])->one();
        $params[] = "LOGIN=" . $login;
        $params[] = "PASSWORD=" . $password;
        $params[] = "TITLE=" . $firm->name;
        $params[] = "SOURCE_ID=2gis";
        $params[] = "COMPANY_TITLE=" . $firm->name;
        $params[] = "ADDRESS=" . @$firm->address;
        $params[] = "WEB_WORK=" . @$website->name;
        $params[] = "EMAIL_WORK=" . @$email->name;
            $firm_rubric = FirmRubric::find()->where(['firm_id' => $firm->id])->all();
            $s_d = "SOURCE_DESCRIPTION=Рубрики: ";
            foreach ($firm_rubric as $f_r) {
                $rubric = Rubric::find()->where(['id' => $f_r->rubric_id])->one();
                $s_d .= $rubric->name." / ";
            }
        $params[] = $s_d;
        $params[] = "PHONE_WORK=" . @$phone[0]->name;
        $params[] = "PHONE_MOBILE=" . @$phone[1]->name;
        $params[] = "PHONE_HOME=" . @$phone[2]->name;
        $params[] = "PHONE_OTHER=" . @$phone[3]->name;
        $params[] = "PHONE_FAX=" . @$phone[4]->name;
        $getParams = implode('&', $params);
        $url = "https://bki.bitrix24.ru/crm/configs/import/lead.php?";
        if($ch = curl_init()){
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS,$getParams);
            curl_exec($ch);
            curl_close($ch);
        }
    }
    }
}

