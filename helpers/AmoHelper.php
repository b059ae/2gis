<?php
namespace app\helpers;


use app\models\Kontragent;
use app\models\Region;
use yii\base\Object;

class AmoHelper extends Object
{

    /**
     * @var string ID рубрики
     */
    public $rubric_id;
    /**
     * @var integer ID региона
     */
    public $region_id;
    /**
     * @var integer Время создания файла
     */
    public $time;
    /**
     * @var int Количество записей на странице
     */
    public $pageSize = 50;

    protected function get($page = 1)
    {
        $url = "https://catalog.api.2gis.ru/2.0/catalog/branch/list?page=" . $page . "&page_size=" . $this->pageSize . "&rubric_id=" . $this->rubric_id . "&hash=db6193842c80e741&stat%5Bpr%5D=3&region_id=" . $this->region_id . "&fields=items.region_id%2Citems.adm_div%2Citems.contact_groups%2Citems.flags%2Citems.address%2Citems.rubrics%2Citems.name_ex%2Citems.point%2Citems.external_content%2Citems.schedule%2Citems.org%2Citems.ads.options%2Citems.reg_bc_url%2Crequest_type%2Cwidgets%2Cfilters%2Citems.reviews%2Ccontext_rubrics%2Chash%2Csearch_attributes&key={apiKey}";
        return \Yii::$app->parser->exec($url);
    }


    public function send(){
        $page = 1;
        do {
            $firms = $this->get($page);
            if (!empty($firms->result->items) && !empty($firms->result)) {
                foreach ($firms->result->items as $itemFirm) {
                    $firm = [];
                    $phone = [];
                    $website = [];
                    $email = [];

                    foreach ($itemFirm->contact_groups as $contact_group) {
                        foreach ($contact_group->contacts as $contact) {
                            if (in_array($contact->type, ['phone', 'website', 'email'])) {
                                ${$contact->type}[] = $contact->text;
                            }
                        }
                    }
                    $ph = '';
                    $web = '';
                    $em = '';
                    foreach ($phone as $p) {
                        $ph .= $p . ', ';
                    }
                    foreach ($website as $w) {
                        $web .= $w . ', ';
                    }
                    foreach ($email as $e) {
                        $em .= $e . ', ';
                    }
                    $regionName = Region::find()->where(['id' => $itemFirm->region_id])->one();
                    $firm = [
                        'name' => $itemFirm->name,
                        'address' => $regionName->name . " " . @$itemFirm->address_name . " " . @$itemFirm->address_comment,
                        'phone' => $ph,
                        'email' => $em,
                        'website' => $web,
                        'index' => @$itemFirm->address->postcode,
                    ];
                    $check = Kontragent::find()->where(['LIKE', 'full_name', $firm['name']])->limit(1)->exists();
                    if (!$check) {
                        $this->sendToAmo($firm);
                    }
                }
            }
            $total = 0;
            if(!empty($firms->result->total)) {
                $total = $firms->result->total;
            }
        } while ($total > $page++ * $this->pageSize);
    }

    /**
     * @param $firm array массив полей для добавления
     */
    public function sendToAmo($firm){
        \Yii::$app->amo->integrate();
        $lead = $this->getLead($firm);
        $leadId = \Yii::$app->amo->addLead($lead);
        $company = $this->getCompany($firm, $leadId);
        \Yii::$app->amo->addCompany($company);
    }

    /**
     * Заполнение массива по компаниям
     * @return array
     */
    public function getCompany($firm, $leadId){
        $company['request']['contacts']['add'] = [
            [
                'name' => $firm['name'],
                "tags" => "2gis",
                "linked_leads_id" => [
                    $leadId
                ],
                'custom_fields' => [
                    [
                        'id' => 303349,// id телефон
                        'name' => 'Телефон',
                        'values' => [
                            [
                                'value' => !empty($firm['phone']) ? $firm['phone'] : null,
                                'enum' => 'WORK'
                            ]
                        ]
                    ],
                    [
                        'id' => 303351,// id email
                        'name' => 'Email',
                        'values' => [
                            [
                                'value' => !empty($firm['email']) ? $firm['email'] : null,
                                'enum' => 'WORK'
                            ]
                        ]
                    ],
                    [
                        'id' => 303353,// id web
                        'name' => 'Web',
                        'values' => [
                            [
                                'value' => !empty($firm['website']) ? $firm['website'] : null,
                                'enum' => 'WORK'
                            ]
                        ]
                    ],
                    [
                        'id' => 533263,// id Почтовый адрес
                        'name' => 'Почтовый адрес',
                        'values' => [
                            [
                                'value' => !empty($firm['index']) ? $firm['index'] : null,
                            ]
                        ]
                    ],
                    [
                        'id' => 303357,// id адрес
                        'name' => 'Адрес',
                        'values' => [
                            [
                                'value' => !empty($firm['address']) ? $firm['address'] : null,
                            ]
                        ]
                    ],
                    [
                        'id' => 352805,// id полное наименование организации
                        'name' => 'Полное наименование организации',
                        'values' => [
                            [
                                'value' => !empty($firm['name']) ? $firm['name'] : null,
                            ]
                        ]
                    ],
                ]
            ]
        ];
        return $company;
    }

    /**
     * Заполнение массива по лидам
     * @return array
     */
    public function getLead($firm){
        $lead['request']['leads']['add'] = [
            [
                'name' => $firm['name'],
                'status_id' => 14256796,// id - Первичный контакт
                'responsible_user_id' => 1372702,// id ответсвенного пользователя: Наташа - 1372696, Настя - 1372711, Аня - 1372702
                "tags" => "2gis",
                'custom_fields' => [
                    [
                        'id' => 303349, // id телефона
                        'values' => [
                            'value' => !empty($firm['phone']) ? $firm['phone'] : null,
                            'enum' => 'WORK'
                        ]
                    ],
                    [
                        'id' => 352803, // id наименования
                        'values' => [
                            'value' => $firm['name'],
                        ]
                    ]
                ]
            ]
        ];
        return $lead;
    }
}