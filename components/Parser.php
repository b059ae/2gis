<?php

namespace app\components;

use yii\base\Component;
use yii\base\Exception;

/**
 * Парсер сайта 2gis.ru
 *
 */
class Parser extends Component
{
    /**
     * @var string API ключ
     */
    public $apiKey;

    /**
     * Получаем страницу с помощью CURL
     * @param string $url
     * @return mixed
     */
    public function exec($url)
    {
        $uagent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";

        $url = strtr($url, [
            '{apiKey}' => $this->apiKey,
        ]);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);   // возвращает веб-страницу
        curl_setopt($ch, CURLOPT_HEADER, 0);           // не возвращает заголовки
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);   // не переходит по редиректам
        curl_setopt($ch, CURLOPT_ENCODING, "");        // обрабатывает все кодировки
        curl_setopt($ch, CURLOPT_USERAGENT, $uagent);  // useragent
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30); // таймаут соединения
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);        // таймаут ответа

        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
//            $header  = curl_getinfo( $ch );
        curl_close($ch);

        if ($err) {
            throw new Exception($errmsg);
        }

        $json = json_decode($content);
        if ($errmsg = json_last_error()) {
            throw new Exception($errmsg);
        }

        return $json;
    }

}
