<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.03.17
 * Time: 9:15
 */

namespace app\components;


use app\models\Firm;
use Exception;
use yii\base\Object;
use yii\helpers\VarDumper;

class FirmsHelper extends Object
{
    /**
     * @var string ID рубрики
     */
    public $rubric_id;
    /**
     * @var integer ID региона
     */
    public $region_id;

    public $tableName = 'firm';
    /**
     * @var string Название компонента БД
     */
    public $db = 'db';

    /**
     * @var int Количество записей на странице
     */
    public $pageSize = 50;

    /**
     * Получение списка фирм
     *
     */

    protected function get($page = 1)
    {
        $url = "https://catalog.api.2gis.ru/2.0/catalog/branch/list?page=" . $page . "&page_size=" . $this->pageSize . "&rubric_id=" . $this->rubric_id . "&region_id=" . $this->region_id . "&hash=db6193842c80e741&stat%5Bpr%5D=3&fields=items.region_id%2Citems.adm_div%2Citems.contact_groups%2Citems.flags%2Citems.address%2Citems.rubrics%2Citems.name_ex%2Citems.point%2Citems.external_content%2Citems.schedule%2Citems.org%2Citems.ads.options%2Citems.reg_bc_url%2Crequest_type%2Cwidgets%2Cfilters%2Citems.reviews%2Ccontext_rubrics%2Chash%2Csearch_attributes&key={apiKey}";
        return \Yii::$app->parser->exec($url);
    }

    /**
     * Создание справочника фирм
     *
     */
    public function insertFirm()
    {
        $page = 1;
        $connection = \Yii::$app->{$this->db};
        $time = time();
        do {
            $firms = $this->get($page);
            foreach ($firms->result->items as $itemFirm) {
                $firm = Firm::find()->where(['gis2_id' => $itemFirm->org->id])->one();
                if ($firm !== null) {
                    continue;//Уже есть у нас такая фирма
                }
                $jsonFirms = json_encode($itemFirm);
                $firm = new Firm();
                $firm->setAttributes([
                    'gis2_id' => $itemFirm->org->id,
                    'region_id' => $itemFirm->region_id,
                    'name' => $itemFirm->name,
                    'address' => @$itemFirm->address_name . @$itemFirm->address_comment,
                    'json' => $jsonFirms,
                    'created_at' => $time
                ]);

                if (!$firm->save()) {
                    throw new Exception('Ошибка при сохранении firm ' . VarDumper::dumpAsString($firm->errors));
                }

                $phone = [];
                $website = [];
                $email = [];

                foreach ($itemFirm->contact_groups as $contact_group) {
                    foreach ($contact_group->contacts as $contact) {
                        if (in_array($contact->type, ['phone', 'website', 'email'])) {
                            ${$contact->type}[] = $contact->value;
                        }

                    }
                }
                $firm = Firm::find()->where(['gis2_id' => $itemFirm->org->id])->one();
                foreach (['phone', 'website', 'email'] as $contactType) {
                    if (count(${$contactType}) > 0) {

                        $connection->createCommand()->batchInsert($contactType, [
                            'name', 'firm_id', 'created_at'
                        ],
                            array_map(function ($item) use ($time, $firm) {
                                return [
                                    $item,
                                    $firm->id,
                                    $time
                                ];
                            }, ${$contactType}))->execute();
                    }
                }

                $connection->createCommand()->batchInsert('firm_rubric', [
                    'firm_id', 'rubric_id'
                ],
                    array_map(function ($item) use ($firm) {
                        return [
                            $firm->id,
                            $item->id
                        ];
                    }, $itemFirm->rubrics))->execute();
            }
        } while ($firms->result->total > $page++ * $this->pageSize);

    }
}