<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 03.03.17
 * Time: 10:06
 */

namespace app\components;


use app\models\Rubric;
use yii\base\Object;

class RubricHelper extends Object
{
    public $tableName = 'rubric';
    /**
     * @var integer ID региона
     */
    public $region_id;
    /**
     * @var string Название компонента БД
     */
    public $db = 'db';

    /**
     * Получение списка рубрик
     *
     */
    protected function get()
    {
        $url = "https://catalog.api.2gis.ru/2.0/catalog/rubric/list?parent_id=0&region_id=".$this->region_id."&sort=popularity&fields=items.rubrics&key={apiKey}";
        return \Yii::$app->parser->exec($url);
    }

    /**
     * Создание справочника рубрик
     *
     */
    public function fillRubrics()
    {
        var_dump($this->region_id);
        $rubrics = $this->get();
        $connection = \Yii::$app->{$this->db};
        $time = time();
        // Заполнение родительских рубрик
        if (!empty($rubrics->result->items)) {
            foreach ($rubrics->result->items as $items) {
                // Проверка наличия рубрики в бд
                if ($this->check($items)) {
                    continue;
                }
                if (!empty($items)) {
                    $connection->createCommand()->insert($this->tableName, [
                        'name' => $items->name,
                        'parent_id' => null,
                        'id' => $items->id,
                        'region_id' => $this->region_id,
                        'created_at' => $time
                    ])->execute();
                }
                $arrRubric = [];
                // Заполнение дочерних рубрик
                foreach ($items->rubrics as $rubric) {
                    if ($this->check($rubric)) {
                        continue;
                    }
                    if (!empty($rubric)) {
                        $arrRubric[] = [$rubric->name, $rubric->parent_id, $rubric->id, $this->region_id, $time];
                    }
                }
                $connection->createCommand()->batchInsert($this->tableName, ['name', 'parent_id', 'id', 'region_id', 'created_at'],
                    $arrRubric
                )->execute();
            }
        }
    }


    public function check($items){
        // Проверка наличия рубрики в бд
        $rubricCheckId = Rubric::find()->where(['id' => $items->id])->one();
        $rubricCheckName = Rubric::find()->where(['name' => $items->name])->one();
        if ($rubricCheckId !== null && $rubricCheckName !== null) {
            return true;
        }
        return false;
    }
}