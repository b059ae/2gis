<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.03.17
 * Time: 9:05
 */

namespace app\components;

/**
 * Класс для сохраниения в csv
 * Class SaveToCSV
 * @package app\components
 */
class SaveToCSV
{
    /**
     * Запись полей в файл csv
     * @param $fields array Поля для заполнения
     */
    public function saveToCSV($fields, $time){
        $fileName = $this->createFileName($time);
        $file = fopen($fileName, 'a');
        fputcsv($file, ['Регион', 'Наименование', 'Адрес','Телефон', 'E-mail', 'Сайт'], '|');
        foreach ($fields as $field) {
            fputcsv($file, $field, '|');
        }
        fclose($file);
    }

    protected function createFileName($time){
        $fileName = 'files/parse_firms_'.$time.'.csv';
        return $fileName;
    }
}