<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 02.03.17
 * Time: 14:11
 */

namespace app\components;



use app\models\Region;
use app\models\Rubric;
use yii\base\Object;

class ExportCsvHelper extends Object
{
    /**
     * @var string ID рубрики
     */
    public $rubric_id;
    /**
     * @var integer ID региона
     */
    public $region_id;
    /**
     * @var integer Время создания файла
     */
    public $time;
    /**
     * @var int Количество записей на странице
     */
    public $pageSize = 50;

    protected function get($page = 1)
    {
        $url = "https://catalog.api.2gis.ru/2.0/catalog/branch/list?page=" . $page . "&page_size=" . $this->pageSize . "&rubric_id=" . $this->rubric_id . "&hash=db6193842c80e741&stat%5Bpr%5D=3&region_id=" . $this->region_id . "&fields=items.region_id%2Citems.adm_div%2Citems.contact_groups%2Citems.flags%2Citems.address%2Citems.rubrics%2Citems.name_ex%2Citems.point%2Citems.external_content%2Citems.schedule%2Citems.org%2Citems.ads.options%2Citems.reg_bc_url%2Crequest_type%2Cwidgets%2Cfilters%2Citems.reviews%2Ccontext_rubrics%2Chash%2Csearch_attributes&key={apiKey}";
        return \Yii::$app->parser->exec($url);
    }

    public function csv()
    {
        $page = 1;
        $firmForCsv = [];
        do {
            $firms = $this->get($page);
            if (!empty($firms->result->items) && !empty($firms->result)) {
                foreach ($firms->result->items as $itemFirm) {

                    $phone = [];
                    $website = [];
                    $email = [];

                    foreach ($itemFirm->contact_groups as $contact_group) {
                        foreach ($contact_group->contacts as $contact) {
                            if (in_array($contact->type, ['phone', 'website', 'email'])) {
                                ${$contact->type}[] = $contact->text;
                            }
                        }
                    }
                    $ph = '';
                    $web = '';
                    $em = '';
                    foreach ($phone as $p) {
                        $ph .= $p . ', ';
                    }
                    foreach ($website as $w) {
                        $web .= $w . ', ';
                    }
                    foreach ($email as $e) {
                        $em .= $e . ', ';
                    }
                    $regionName = Region::find()->where(['id' => $itemFirm->region_id])->one();
                    $firmForCsv[] = [
                        'region_id' => $regionName->name,
                        'name' => $itemFirm->name,
                        'address' => @$itemFirm->address_name . @$itemFirm->address_comment,
                        'phone' => $ph,
                        'email' => $em,
                        'website' => $web,
                    ];
                }
            }
            $total = 0;
            if(!empty($firms->result->total)) {
                $total = $firms->result->total;
            }
        } while ($total > $page++ * $this->pageSize);
        if (!empty($firmForCsv)) {
            $rubricName = Rubric::find()->where(['id' => $this->rubric_id])->one();
            var_dump('Записано по '.$this->region_id.' региону. Рубрика '.$rubricName->name);
            $save = new SaveToCSV();
            $save->saveToCSV($firmForCsv, $this->time);
        }
    }
}